module.exports = (grunt) ->
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-stylus')
  grunt.loadNpmTasks('grunt-contrib-jade')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.initConfig

    watch:
      coffee:
        files: "#{__dirname}/src/coffee/*.coffee"
        tasks: ['coffee:compile']
        options:
          livereload: true
      stylus:
        files: "#{__dirname}/src/stylus/*.styl"
        tasks: ['stylus:compile']
        options:
          livereload: true
      jade:
        files: "#{__dirname}/src/jade/*.jade"
        tasks: ['jade:compile']
        options:
          livereload: true

    stylus:
      compile:
        options:
          compress: true
          use: [require('jeet')]
        files:
          "dist/assets/css/style.css": "src/stylus/style.styl"

    coffee:
      compile:
        options:
          bare: true
        expand: true,
        flatten: true,
        cwd: "#{__dirname}/src/coffee/",
        src: ['*.coffee'],
        dest: "#{__dirname}/dist/assets/js/",
        ext: '.js'

    jade:
      compile:
        files:
          "dist/index.html": "src/jade/index.jade"

  grunt.registerTask 'default', ['watch']
